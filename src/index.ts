import { App } from "./app";

const port = process.env.PORT || 3000;

new App().express.listen(port, (): void => {
  return console.log(`server is running on port: ${port}`);
});

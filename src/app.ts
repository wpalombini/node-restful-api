import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import { OrderRoutes } from "./v1/routes/order.route";
import { ProductRoutes } from "./v1/routes/product.route";
import { ResponseDto } from "./v1/dtos/response.dto";
import { Auth } from "./auth";
import { ExceptionRoutes } from "./v1/routes/exceptions.route";

export class App {
  public express: express.Express;

  private auth: Auth;
  private exceptionRoutes: ExceptionRoutes;
  private ordersRoutes: OrderRoutes;
  private productsRoutes: ProductRoutes;

  constructor() {
    this.auth = new Auth();
    this.exceptionRoutes = new ExceptionRoutes();
    this.ordersRoutes = new OrderRoutes();
    this.productsRoutes = new ProductRoutes();
    this.express = express();
    this.setupApp();
    this.mountRoutes();
    this.handleErrors();
  }

  private setupApp(): void {
    // parse application/x-www-form-urlencoded
    this.express.use(bodyParser.urlencoded({ extended: false }));

    // parse application/json
    this.express.use(bodyParser.json());

    // enable CORS
    this.express.use(
      cors({ origin: ["http://localhost:8080", "https://localhost:8080"] })
    );

    // authentication
    this.express.use(this.auth.checkJwt.unless(this.exceptionRoutes.exclude));

    // authorization
    this.express.use((<any>this.auth.checkScopes).unless(this.exceptionRoutes.exclude));
  }

  private mountRoutes(): void {
    this.express.use("/v1/orders", this.ordersRoutes.orderRoute);
    this.express.use("/v1/products", this.productsRoutes.productRoute);
  }

  private handleErrors(): void {
    this.express.use((req, res, next) => {
      const err: any = new Error("Not found.");
      err.status = 404;
      next(err);
    });

    this.express.use((err, req, res, next) => {
      res
        .status(err.status || 500)
        .json(new ResponseDto(false, err.message, null));
    });
  }
}

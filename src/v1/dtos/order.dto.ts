export class OrderDto {
    id: number;
    externalId: string;
    totalAmount: number;
    clientId: string;
    createdAt: Date;
}
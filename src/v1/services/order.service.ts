import { Observable, of } from "rxjs";
import { map, tap } from "rxjs/operators";
import { OrderDto } from "../dtos/order.dto";
import { orders } from "./data";

export class OrderService {

  public getOrdersAsync(clientId: string, externalId: string): Observable<OrderDto[]> {

    return of(orders)
      .pipe(
        map((orders: OrderDto[]) => {
          let result = orders.filter((order) => order.clientId == clientId);

          if (externalId) {
            result = result.filter((order) => order.externalId == externalId);
          }

          return result;
        }),
      );
  }

  public saveOrderAsync(order: OrderDto): Observable<OrderDto> {

    return of(order)
      .pipe(
        tap((order: OrderDto) => {
          const currentMaxId: number = Math.max(...orders.map((order: OrderDto) => order.id));
          order.id = currentMaxId + 1;
          order.createdAt = new Date();
          orders.push(order);
        }),
      );
  }
}

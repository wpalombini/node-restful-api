import { OrderService } from "./order.service";
import { OrderDto } from "../dtos/order.dto";
import { Observable, Subscription } from "rxjs";

describe("order service tests:", () => {
  let orderService: OrderService;

  beforeEach(() => {
    orderService = new OrderService();
  });

  describe("getOrdersAsync:", () => {
    test("should return orders for client Id", (done: jest.DoneCallback) => {
      const clientId: string = "9qyWt5I9uF4RenPXj9yEpw2p6uKI3wtu";

      const orders$: Observable<OrderDto[]> = orderService.getOrdersAsync(
        clientId,
        null
      );

      const subscription: Subscription = orders$.subscribe((orders) => {
        expect(orders).toBeTruthy();
        expect(orders).toHaveLength(5);
        orders.forEach((order: OrderDto) => {
          expect(order.clientId).toBe(clientId);
        });
        done();
      });

      expect(subscription.closed).toBe(true);
    });
  });
});

import { OrderDto } from "../dtos/order.dto";

export let orders: OrderDto[] = [
  {
    id: 1,
    externalId: "364040d5-6a57-448d-2d6a-08d6195ade6a",
    totalAmount: 10,
    clientId: "9qyWt5I9uF4RenPXj9yEpw2p6uKI3wtu",
    createdAt: new Date(2018, 10, 5),
  },
  {
    id: 2,
    externalId: "364040d5-6a57-448d-2d6a-08d6195ade6a",
    totalAmount: 20,
    clientId: "9qyWt5I9uF4RenPXj9yEpw2p6uKI3wtu",
    createdAt: new Date(2018, 10, 24),
  },
  {
    id: 3,
    externalId: "d074709d-d5ae-4b09-009a-08d61df7882f",
    totalAmount: 30,
    clientId: "9qyWt5I9uF4RenPXj9yEpw2p6uKI3wtu",
    createdAt: new Date(2018, 10, 25),
  },
  {
    id: 4,
    externalId: "d074709d-d5ae-4b09-009a-08d61df7882f",
    totalAmount: 40,
    clientId: "9qyWt5I9uF4RenPXj9yEpw2p6uKI3wtu",
    createdAt: new Date(2018, 10, 26),
  },
  {
    id: 5,
    externalId: "d074709d-d5ae-4b09-009a-08d61df7882f",
    totalAmount: 50,
    clientId: "9qyWt5I9uF4RenPXj9yEpw2p6uKI3wtu",
    createdAt: new Date(2018, 10, 27),
  },
];

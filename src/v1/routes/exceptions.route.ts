export class ExceptionRoutes {
  public exclude = {
    path: [
      {
        url: "/v1/products",
        method: "GET",
      },
    ],
  };
}

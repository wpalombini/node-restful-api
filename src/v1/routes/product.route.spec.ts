import * as supertest from "supertest";
import { App } from "../../app";
import { Server } from "http";
import { ResponseDto } from "../dtos/response.dto";

describe("product route tests:", () => {
  let request: supertest.SuperTest<supertest.Test>;
  let server: Server;
  beforeEach(() => {
    server = new App().express.listen();
    request = supertest(server);
  });
  afterEach(() => {
    server.close();
  });

  describe("/v1/products", () => {
    test("should return array of products", async (done: jest.DoneCallback) => {
      await request
        .get("/v1/products")
        .expect(200)
        .expect(
          JSON.stringify(
            new ResponseDto(true, "Hello from GET /products", {
              products: [4, 5, 6],
            })
          )
        );
      done();
    });
  });
});

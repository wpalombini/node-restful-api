import * as supertest from "supertest";
import { App } from "../../app";
import { Server } from "http";
import { ResponseDto } from "../dtos/response.dto";

describe("order route tests:", () => {
  let request: supertest.SuperTest<supertest.Test>;
  let server: Server;
  beforeEach(() => {
    server = new App().express.listen();
    request = supertest(server);
  });
  afterEach(() => {
    server.close();
  });

  describe("/v1/orders", () => {
    test("should not be authorized", async (done: jest.DoneCallback) => {
      await request
        .get("/v1/orders")
        .expect(401)
        .expect(
          JSON.stringify(
            new ResponseDto(false, "No authorization token was found", null)
          )
        );
      done();
    });
  });
});

import * as express from "express";
import { ResponseDto } from "../dtos/response.dto";

export class ProductRoutes {
  public productRoute: express.Router;

  constructor() {
    this.mountRoutes();
  }

  private mountRoutes() {
    this.productRoute = express.Router();

    this.productRoute.get("/", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      res.status(200).json(
        new ResponseDto(true, "Hello from GET /products", {
          products: [4, 5, 6],
        })
      );
    });
  }
}

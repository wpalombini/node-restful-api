import * as express from "express";
import { ResponseDto } from "../dtos/response.dto";
import { OrderService } from "../services/order.service";
import { OrderDto } from "../dtos/order.dto";
import { BaseRoute } from "./base.route";

export class OrderRoutes extends BaseRoute {
  public orderRoute: express.Router;

  private orderService: OrderService;

  constructor() {
    super();
    this.orderService = new OrderService();
    this.mountRoutes();
  }

  private mountRoutes() {
    this.orderRoute = express.Router();

    this.orderRoute.get("/", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      const clientId: string = this.getClientId(req);
      const externalId: string = req.query["animalId"] as string;

      this.orderService.getOrdersAsync(clientId, externalId)
        .subscribe((order) => {
          res
            .status(200)
            .json(new ResponseDto(true, "Hello from GET /orders", order));
        },
          undefined, // handle error here
          () => console.log("Successfully unsubscribed!"));
    });

    this.orderRoute.post("/", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      const clientId: string = this.getClientId(req);

      const order = new OrderDto();
      order.clientId = clientId;
      order.totalAmount = req.body.totalAmount;

      this.orderService.saveOrderAsync(order)
        .subscribe((order: OrderDto) => {
          res
            .status(201)
            .json(new ResponseDto(true, "Hello from POST /orders", order));
        });
    });
  }
}

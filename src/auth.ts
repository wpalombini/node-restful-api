import * as jwt from "express-jwt";
import * as jwtPermissions from "express-jwt-permissions";
import * as jwksRsa from "jwks-rsa";

export class Auth {
  // Authentication middleware. When used, the
  // Access Token must exist and be verified against
  // the Auth0 JSON Web Key Set
  public checkJwt: jwt.RequestHandler = jwt({
    // Dynamically provide a signing key
    // based on the kid in the header and
    // the signing keys provided by the JWKS endpoint.
    secret: jwksRsa.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://wpalombini.au.auth0.com/.well-known/jwks.json`,
    }),

    // Validate the audience and the issuer.
    audience: "https://wpalombini.au.auth0.com/api/v2/",
    issuer: `https://wpalombini.au.auth0.com/`,
    algorithms: ["RS256"],
  });

  public checkScopes = jwtPermissions({
    permissionsProperty: "scope",
  }).check(["read:users"]);
}
